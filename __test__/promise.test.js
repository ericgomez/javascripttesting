import { DataFromApi } from '../promise'

describe("Probando promesas", () => {
    test("Realizando una petición a una api", done => {
        const api = "https://rickandmortyapi.com/api/character";
        DataFromApi(api).then(data => {
            expect(data.results.length).toBeGreaterThan(0);
            done();
        });
    });

    //Promesas resueltas (resolve)
    test('Resuelve un Hola!', () => {
        return expect(Promise.resolve('Hola!')).resolves.toBe('Hola!')
    })

    //Promesas rechazadas (reject)
    test('Rechaza con un error', () => {//Ejemplo 1
        return expect(Promise.reject(new Error('Error'))).rejects.toThrow('Error')
    })
    test('Rechaza con un error', () => {//Ejemplo 2
        return expect(Promise.reject('Error')).rejects.toBe('Error')
    })
});