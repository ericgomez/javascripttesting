describe('comparadores comunes', () => {
    const user = {
        name: "Eric",
        lastname: "Gomez"
    }
    const user2 = {
        name: "Eric",
        lastname: "Gomez"
    }
    test('Igualdad de elementos ', () => {
        expect(user).toEqual(user2);
    });
    test('No son iguales los elementos ', () => {
        expect(user).not.toEqual(user2);
    });
})