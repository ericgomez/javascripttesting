describe('Comprobar cadenas de texto', () => {

    const text = 'un bonito texto'

    test('debe contener el siguiente texto', () => {
        expect(text).toMatch(/bonito/)//Verificar si contiene una palabra
    })

    test('No contiene el siguiente texto', () => {
        expect(text).not.toMatch(/es/)//Verificar si contiene una palabra
    })

    test('Comprobar el tamaño de un texto', () => {
        expect(text).toHaveLength(15)
    })
})