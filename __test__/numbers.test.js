import { numbers } from '../numbers'

describe('Comparacion de numeros', () => {
    test('Mayor que', () => {
        expect(numbers(2,2)).toBeGreaterThan(3)//Comparamos resultados mayores
    })
    test('Mayor que o igual', () => {
        expect(numbers(2,2)).toBeGreaterThanOrEqual(4)//Comparamos resultados mayores o iguales
    })
    test('Menor que', () => {
        expect(numbers(2,2)).toBeLessThan(5)//Comparamos resultados menores
    })
    test('Menor que o igual', () => {
        expect(numbers(2,2)).toBeLessThanOrEqual(5)//Comparamos resultados menores o iguales
    })
    test('Numeros flotantes', () => {
        expect(numbers(0.2,0.2)).toBeCloseTo(0.4)//Comparamos resultados flotantes
    })
});